-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2016.2.3-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 24860fcf28517431be055b7b8f20b0dea8b08f84 5637 intellij-idea-community_2016.2.3.orig.tar.gz
 a5450db394e9830ab86a81322ff33430df62069f 5648 intellij-idea-community_2016.2.3-1.debian.tar.xz
Checksums-Sha256:
 41958f809fdb7ce6640143c04c88233d51ab86e66be5c0a9e30e846af332550b 5637 intellij-idea-community_2016.2.3.orig.tar.gz
 ffa5a0166ca5c761cbf02267ff7e6b98f58472ce4a205d147d99657864248f65 5648 intellij-idea-community_2016.2.3-1.debian.tar.xz
Files:
 e105950dd78a2260639bd06b848e4f3d 5637 intellij-idea-community_2016.2.3.orig.tar.gz
 83382aad351a30756b7f8e97da9b2a96 5648 intellij-idea-community_2016.2.3-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJXy0apAAoJECuF2UqwCcGYFlYP/jgAK6l7lgPsdjSX1d0dNH2d
+TGwq7tAjDWQJl3KbJVsP/WwnESZy08de9jXGuj3w3DrG4RItTdb/SRH8Fb+CS3L
5T5qG3FzmPkVjjjYLZyFtUSlFR4UEgVcf0LeZZYhjTVzrQ7WJ6MdUBtdC5lReG3D
ZIdgX5evdKIrcRhR5TI9jukYpo5HhngtauRIEDg+P7x//DJl/36aogUNa/QDsAhr
7qigV6MhEH30xTgHil5Um/wgnIjyQTTHCXs73SNkchGTZbhNY7DleMTBxv3EaYWQ
wg6zJtJnh3l0lRumDZR9TZjodlib4lD1QIN2RYOUbujqO8jwAsGwh9bG44CRP3Bd
GbrNJiwIeVJWgD5cHMcSeRsOmm5/jpnEf7+73+jl0+E74BSV0jwPKlHs1RFwAMzu
GcAiGsR4CTni5FTGKumMMeLb0gJgs2f6AOuyH0+Flij7X++omvgn32U3CDGzzWzT
iC2ycTh84OIlBqogxDTPeFzeIzeRYXPhuD84s1wS67d+WpDHo4z7F04cyPp4k/5x
PQAkPhqkKYuefGL72vc4Dd0k8uKerfU4CbQYiUcb6OXHJPX71gVyYZujGbxoypaK
BHnECSYI/w1pkDb+Pf8bfBF0rLx4yFFYdGgUqK/kuIfNZk+3oDqJYpn2uLEkwUlx
u+u+B7k55RYLHucG6EoZ
=ekhz
-----END PGP SIGNATURE-----
